// import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:stembox_tutorial/screens/equipments_screen/equipments_screen.dart';
import 'package:stembox_tutorial/screens/modules_screen/modules_screen.dart';
import 'package:stembox_tutorial/screens/script_commands_screen/script_commands_screen.dart';
import 'dart:async';
import 'screens/first_screen/first_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyCFtlfG-jPrPeGIhnWqZHAEfhackIpMBVc",
          authDomain: "stembox-tutorial.firebaseapp.com",
          projectId: "stembox-tutorial",
          storageBucket: "stembox-tutorial.appspot.com",
          messagingSenderId: "42992717667",
          appId: "1:42992717667:web:e3ba777077c6615446cd56",
          measurementId: "G-2QLMCWG0WG"));

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      initialRoute: "/",
      routes: {
        "/": (context) => FirstScreen(),
        "/modules": (context) => ModulesScreen(),
        "/scriptcommands": (context) => ScriptCommandsScreen(),
        "/equipments": (context) => EquipmentsScreen(),
      },
    );
  }
}
