import 'package:flutter/material.dart';
import 'package:stembox_tutorial/screens/first_screen/animation_util.dart';
import 'package:stembox_tutorial/screens/first_screen/views/intro_view.dart';
import 'package:stembox_tutorial/screens/first_screen/views/panel_view.dart';

class FirstScreen extends StatefulWidget {
  final skipMode;
  const FirstScreen({super.key, this.skipMode = false});

  @override
  State<FirstScreen> createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen>
    with TickerProviderStateMixin {
  late final AnimationController _controller =
      CustomAnimations.createController(this, 1000);

  late final AnimationController _controller2 =
      CustomAnimations.createController(this, 800);

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    startAnimation();
    super.initState();
  }

  bool _panelVisible = false;

  void startAnimation() {
    if (widget.skipMode) {
      setState(() {
        _panelVisible = true;
      });
    } else {
      Future.delayed(const Duration(seconds: 3), () {
        _controller.forward().then((value) {
          Future.delayed(const Duration(seconds: 3), () {
            _controller2.forward().then((value) {
              setState(() {
                _panelVisible = true;
              });
            });
          });
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.black,
      child: !_panelVisible
          ? introView(
              animationController: _controller,
              animationController2: _controller2,
            )
          : PanelView(
              panelVisible: _panelVisible,
            ),
    );
  }
}
