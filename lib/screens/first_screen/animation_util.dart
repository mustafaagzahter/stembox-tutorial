import 'package:flutter/material.dart';

class CustomAnimations {
  static AnimationController createController(
      TickerProviderStateMixin vsync, ms) {
    return AnimationController(
      duration: Duration(milliseconds: ms),
      vsync: vsync,
    );
  }

  static Animation<Offset> createOffsetAnimationIn(
    AnimationController controller,
    bOffset,
    eOffset,
  ) {
    return Tween<Offset>(
      begin: bOffset,
      end: eOffset,
    ).animate(CurvedAnimation(
      parent: controller,
      curve: Curves.easeInCubic,
    ));
  }

  static Animation<Offset> createOffsetAnimationOut(
    AnimationController controller,
    bOffset,
    eOffset,
  ) {
    return Tween<Offset>(
      begin: bOffset,
      end: eOffset,
    ).animate(CurvedAnimation(
      parent: controller,
      curve: Curves.easeOutCubic,
    ));
  }
}
