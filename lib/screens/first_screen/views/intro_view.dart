import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stembox_tutorial/screens/first_screen/animation_util.dart';

class introView extends StatefulWidget {
  const introView({
    super.key,
    required this.animationController,
    required this.animationController2,
  });

  final animationController;
  final animationController2;

  @override
  State<introView> createState() => _introViewState();
}

class _introViewState extends State<introView> with TickerProviderStateMixin {
  late final AnimationController _controller = widget.animationController;
  late final Animation<Offset> _animationIn =
      CustomAnimations.createOffsetAnimationOut(
          _controller, Offset(-10, 0), Offset.zero);
  late final Animation<Offset> _animationIn2 =
      CustomAnimations.createOffsetAnimationOut(
          _controller, Offset(10, 0), Offset.zero);

  late final AnimationController _controller2 = widget.animationController2;
  late final Animation<Offset> _animationOut =
      CustomAnimations.createOffsetAnimationIn(
          _controller2, Offset.zero, Offset(0, -10));
  late final Animation<Offset> _animationOut2 =
      CustomAnimations.createOffsetAnimationIn(
          _controller2, Offset.zero, Offset(0, 10));

  @override
  void initState() {
    // setSettings();
    super.initState();
  }

  void setSettings() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("devMode", false);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(30.0),
      child: FittedBox(
        child: Container(
          height: 700,
          width: 800,
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            SlideTransition(
              position: _animationOut,
              child: SlideTransition(
                position: _animationIn,
                child: Text(
                  "STEMBOX",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 120,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 15),
                ),
              ),
            ),
            SizedBox(height: 50),
            SlideTransition(
              position: _animationOut2,
              child: SlideTransition(
                position: _animationIn2,
                child: Container(
                  color: Colors.white,
                  padding: EdgeInsets.symmetric(horizontal: 50),
                  child: Text(
                    "TUTORIAL",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 120,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 15,
                    ),
                  ),
                ),
              ),
            )
          ]),
        ),
      ),
    );
  }
}
