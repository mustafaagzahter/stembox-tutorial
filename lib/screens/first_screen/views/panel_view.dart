import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stembox_tutorial/screens/first_screen/views/panel_devpass_view.dart';

class PanelView extends StatefulWidget {
  const PanelView({super.key, required this.panelVisible});
  final bool panelVisible;

  @override
  State<PanelView> createState() => _PanelViewState();
}

class _PanelViewState extends State<PanelView> {
  bool hover = false;
  bool hover2 = false;
  bool fadeEffect = false;
  bool fadePanel = false;
  bool devLogin = false;
  Color fadeColor = Colors.black;

  void initState() {
    super.initState();
    startAnimation();
  }

  void startAnimation() {
    Future.delayed(const Duration(microseconds: 500), () {
      setState(
        () {
          fadeEffect = true;
        },
      );
      Future.delayed(const Duration(milliseconds: 1000), () {
        setState(() {
          fadePanel = true;
        });
      });
    });
  }

  void developerLogin() {
    setState(() {
      fadePanel = false;
    });
    Future.delayed(const Duration(milliseconds: 50), () {
      setState(() {
        fadeEffect = false;
      });
      Future.delayed(const Duration(milliseconds: 1000), () {
        setState(() {
          devLogin = true;
        });
      });
    });
  }

  void navigateToModules() async {
    Future.delayed(const Duration(microseconds: 500), () {
      setState(
        () {
          fadePanel = false;
        },
      );
      Future.delayed(const Duration(milliseconds: 1000), () {
        setState(() {
          fadeEffect = false;
        });
        Future.delayed(const Duration(milliseconds: 1300), () {
          setState(() {
            fadeColor = Colors.white;
          });
          Future.delayed(const Duration(milliseconds: 1500), () async {
            final SharedPreferences prefs =
                await SharedPreferences.getInstance();
            prefs.setBool("devMode", false).then((value) =>
                Navigator.of(context).pushNamed("/modules").then((value) {
                  Future.delayed(const Duration(milliseconds: 300), () {
                    setState(() {
                      fadeColor = Colors.black;
                    });
                  });
                }).then(
                  (value) =>
                      Future.delayed(const Duration(milliseconds: 1000), () {
                    startAnimation();
                  }),
                ));
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, _constraints) {
        if (_constraints.maxWidth < 800) {
          hover = true;
          hover2 = true;
        }
        if (!devLogin) {
          return Stack(
            children: [
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: FittedBox(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 30, vertical: 90),
                      child: _constraints.maxWidth >= 800
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                  MouseRegion(
                                    onEnter: (event) {
                                      setState(() {
                                        hover = true;
                                      });
                                    },
                                    onExit: (event) {
                                      setState(() {
                                        hover = false;
                                      });
                                    },
                                    child: GestureDetector(
                                      onTap: () {
                                        developerLogin();
                                      },
                                      child: AnimatedContainer(
                                        duration:
                                            const Duration(milliseconds: 500),
                                        height: 150,
                                        width: 130,
                                        decoration: BoxDecoration(
                                            color: Colors.black,
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            border: Border.all(
                                                color: hover
                                                    ? Colors.white
                                                        .withOpacity(1)
                                                    : Colors.white
                                                        .withOpacity(0),
                                                width: 1),
                                            boxShadow: [
                                              BoxShadow(
                                                  color: hover
                                                      ? Colors.white
                                                          .withOpacity(0.5)
                                                      : Colors.white
                                                          .withOpacity(0.0),
                                                  blurRadius: 10,
                                                  offset: const Offset(0, 0)),
                                            ]),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Icon(
                                              Icons.developer_mode_rounded,
                                              color: Colors.white,
                                              size: 50,
                                              shadows: [
                                                Shadow(
                                                    color: Colors.white
                                                        .withOpacity(0.6),
                                                    blurRadius: 30,
                                                    offset: const Offset(0, 0))
                                              ],
                                            ),
                                            SizedBox(
                                              height: 15,
                                            ),
                                            Text(
                                              "Geliştirici",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.bold,
                                                  shadows: [
                                                    Shadow(
                                                        color: Colors.white
                                                            .withOpacity(0.8),
                                                        blurRadius: 20,
                                                        offset:
                                                            const Offset(0, 0))
                                                  ]),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 100,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      navigateToModules();
                                    },
                                    child: MouseRegion(
                                      onEnter: (event) {
                                        setState(() {
                                          hover2 = true;
                                        });
                                      },
                                      onExit: (event) {
                                        setState(() {
                                          hover2 = false;
                                        });
                                      },
                                      child: AnimatedContainer(
                                        duration:
                                            const Duration(milliseconds: 500),
                                        height: 150,
                                        width: 130,
                                        decoration: BoxDecoration(
                                            color: Colors.black,
                                            borderRadius:
                                                BorderRadius.circular(15),
                                            border: Border.all(
                                                color: hover2
                                                    ? Colors.white
                                                        .withOpacity(1)
                                                    : Colors.white
                                                        .withOpacity(0),
                                                width: 1),
                                            boxShadow: [
                                              BoxShadow(
                                                  color: hover2
                                                      ? Colors.white
                                                          .withOpacity(0.5)
                                                      : Colors.white
                                                          .withOpacity(0.0),
                                                  blurRadius: 10,
                                                  offset: const Offset(0, 0)),
                                            ]),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Icon(
                                              Icons.developer_board_rounded,
                                              color: Colors.white,
                                              size: 50,
                                              shadows: [
                                                Shadow(
                                                    color: Colors.white
                                                        .withOpacity(0.6),
                                                    blurRadius: 30,
                                                    offset: const Offset(0, 0))
                                              ],
                                            ),
                                            SizedBox(
                                              height: 15,
                                            ),
                                            Text(
                                              "Kullanıcı",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold,
                                                shadows: [
                                                  Shadow(
                                                      color: Colors.white
                                                          .withOpacity(0.8),
                                                      blurRadius: 30,
                                                      offset:
                                                          const Offset(0, 0))
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                ])
                          : Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                  GestureDetector(
                                    onTap: () {
                                      developerLogin();
                                    },
                                    child: AnimatedContainer(
                                      duration:
                                          const Duration(milliseconds: 500),
                                      height: 150,
                                      width: 130,
                                      decoration: BoxDecoration(
                                          color: Colors.black,
                                          borderRadius:
                                              BorderRadius.circular(15),
                                          border: Border.all(
                                              color: hover
                                                  ? Colors.white.withOpacity(1)
                                                  : Colors.white.withOpacity(0),
                                              width: 1),
                                          boxShadow: [
                                            BoxShadow(
                                                color: hover
                                                    ? Colors.white
                                                        .withOpacity(0.5)
                                                    : Colors.white
                                                        .withOpacity(0.0),
                                                blurRadius: 10,
                                                offset: const Offset(0, 0)),
                                          ]),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.developer_mode_rounded,
                                            color: Colors.white,
                                            size: 50,
                                            shadows: [
                                              Shadow(
                                                  color: Colors.white
                                                      .withOpacity(0.6),
                                                  blurRadius: 30,
                                                  offset: const Offset(0, 0))
                                            ],
                                          ),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Text(
                                            "Geliştirici",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold,
                                                shadows: [
                                                  Shadow(
                                                      color: Colors.white
                                                          .withOpacity(0.8),
                                                      blurRadius: 20,
                                                      offset:
                                                          const Offset(0, 0))
                                                ]),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 100,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      navigateToModules();
                                    },
                                    child: AnimatedContainer(
                                      duration:
                                          const Duration(milliseconds: 500),
                                      height: 150,
                                      width: 130,
                                      decoration: BoxDecoration(
                                          color: Colors.black,
                                          borderRadius:
                                              BorderRadius.circular(15),
                                          border: Border.all(
                                              color: hover2
                                                  ? Colors.white.withOpacity(1)
                                                  : Colors.white.withOpacity(0),
                                              width: 1),
                                          boxShadow: [
                                            BoxShadow(
                                                color: hover2
                                                    ? Colors.white
                                                        .withOpacity(0.5)
                                                    : Colors.white
                                                        .withOpacity(0.0),
                                                blurRadius: 10,
                                                offset: const Offset(0, 0)),
                                          ]),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.developer_board_rounded,
                                            color: Colors.white,
                                            size: 50,
                                            shadows: [
                                              Shadow(
                                                  color: Colors.white
                                                      .withOpacity(0.6),
                                                  blurRadius: 30,
                                                  offset: const Offset(0, 0))
                                            ],
                                          ),
                                          SizedBox(
                                            height: 15,
                                          ),
                                          Text(
                                            "Kullanıcı",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 18,
                                              fontWeight: FontWeight.bold,
                                              shadows: [
                                                Shadow(
                                                    color: Colors.white
                                                        .withOpacity(0.8),
                                                    blurRadius: 30,
                                                    offset: const Offset(0, 0))
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                ]),
                    ),
                  ),
                ),
              ),
              !fadePanel
                  ? AnimatedContainer(
                      duration: const Duration(milliseconds: 1000),
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      color: fadeEffect
                          ? fadeColor.withOpacity(0)
                          : fadeColor.withOpacity(1),
                    )
                  : Container(),
            ],
          );
        } else {
          return PanelDevPassw();
        }
      },
    );
  }
}
