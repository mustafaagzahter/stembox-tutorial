import 'dart:async';
import 'dart:ui';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stembox_tutorial/screens/first_screen/first_screen.dart';
import '../animation_util.dart';

class PanelDevPassw extends StatefulWidget {
  const PanelDevPassw({super.key});

  @override
  State<PanelDevPassw> createState() => _PanelDevPasswState();
}

class _PanelDevPasswState extends State<PanelDevPassw>
    with TickerProviderStateMixin {
  bool passwAnimation = false;

  TextEditingController passwController = TextEditingController();
  StreamController<ErrorAnimationType>? errorController;

  late final AnimationController animController =
      CustomAnimations.createController(this, 1000);
  late Animation<Offset> _animation = CustomAnimations.createOffsetAnimationOut(
      animController, Offset(-10, 0), Offset.zero);

  Color pinColor = Colors.white;
  Color fadeColor = Colors.black;

  String passw = "";

  @override
  void initState() {
    errorController = StreamController<ErrorAnimationType>();
    firebaseLoad();
    startAnimation();
    super.initState();
  }

  void firebaseLoad() async {
    FirebaseFirestore.instance
        .collection("system")
        .doc("dev")
        .get()
        .then((value) {
      setState(() {
        passw = value["devpassword"];
      });
    });
  }

  void startAnimation() {
    Future.delayed(const Duration(milliseconds: 300), () {
      setState(() {
        passwAnimation = true;
      });
      Future.delayed(const Duration(milliseconds: 1300), () {
        animController.forward();
      });
    });
  }

  void success() {
    Future.delayed(const Duration(milliseconds: 50), () {
      setState(() {
        pinColor = Colors.green;
      });
      Future.delayed(const Duration(milliseconds: 800), () {
        setState(() {
          _animation = CustomAnimations.createOffsetAnimationIn(
              animController, Offset.zero, Offset(10, 0));
        });
        animController.reset();
        animController.forward();
        Future.delayed(const Duration(milliseconds: 1000), () {
          setState(() {
            passwAnimation = false;
          });
          Future.delayed(const Duration(milliseconds: 1300), () {
            setState(() {
              fadeColor = Colors.white;
            });
            Future.delayed(const Duration(milliseconds: 1500), () async {
              final SharedPreferences prefs =
                  await SharedPreferences.getInstance();
              prefs.setBool("devMode", true).then((value) =>
                  Navigator.of(context).pushNamed("/modules").then((value) {
                    setState(() {
                      fadeColor = Colors.black;
                    });
                    Future.delayed(Duration(milliseconds: 1000), () {
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                          builder: (context) => FirstScreen(
                            skipMode: true,
                          ),
                        ),
                      );
                    });
                  }));
            });
          });
        });
      });
    });
  }

  @override
  void dispose() {
    errorController!.close();
    animController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      alignment: Alignment.center,
      children: [
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/devpass_bg.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 30.0, sigmaY: 30.0),
          child: AnimatedContainer(
            duration: Duration(milliseconds: 1000),
            color: passwAnimation
                ? fadeColor.withOpacity(0.9)
                : fadeColor.withOpacity(1),
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: FittedBox(
            child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 30, vertical: 90),
                child: AnimatedContainer(
                  duration: Duration(milliseconds: 900),
                  height: 40,
                  width: 200,
                  child: SlideTransition(
                    position: _animation,
                    child: PinCodeTextField(
                      appContext: context,
                      length: 4,
                      cursorHeight: 15,
                      cursorWidth: 1,
                      obscureText: true,
                      obscuringCharacter: "*",
                      autoFocus: true,
                      keyboardType: TextInputType.number,
                      animationType: AnimationType.scale,
                      textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                      ),
                      errorAnimationController: errorController,
                      pinTheme: PinTheme(
                        borderRadius: BorderRadius.circular(8),
                        borderWidth: 1,
                        shape: PinCodeFieldShape.box,
                        fieldHeight: 40,
                        fieldWidth: 40,
                        disabledColor: pinColor,
                        inactiveColor: pinColor,
                        activeColor: pinColor,
                        selectedColor: pinColor,
                      ),
                      controller: passwController,
                      onCompleted: (value) {
                        if (value == passw) {
                          setState(() {
                            success();
                          });
                        } else {
                          errorController!.add(ErrorAnimationType.shake);
                          Future.delayed(const Duration(milliseconds: 800), () {
                            passwController.clear();
                            FocusScope.of(context).previousFocus();
                          });
                        }
                      },
                    ),
                  ),
                )),
          ),
        ),
      ],
    );
  }
}
