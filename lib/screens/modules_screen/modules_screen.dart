import 'package:flutter/material.dart';
import 'package:stembox_tutorial/screens/first_screen/animation_util.dart';
import 'package:stembox_tutorial/widgets/bottom_logo.dart';

class ModulesScreen extends StatefulWidget {
  const ModulesScreen({Key? key}) : super(key: key);

  @override
  _ModulesScreenState createState() => _ModulesScreenState();
}

class _ModulesScreenState extends State<ModulesScreen>
    with TickerProviderStateMixin {
  bool hover = false;
  bool hover2 = false;

  late final AnimationController animController =
      CustomAnimations.createController(this, 1000);

  late final Animation<Offset> leftpaneloffset =
      CustomAnimations.createOffsetAnimationOut(
          animController, Offset(-10, 0), Offset.zero);

  late final Animation<Offset> rightpaneloffset =
      CustomAnimations.createOffsetAnimationOut(
          animController, Offset(10, 0), Offset.zero);

  late final Animation<Offset> logooffset =
      CustomAnimations.createOffsetAnimationOut(
          animController, Offset(0, 10), Offset.zero);

  void initState() {
    startAnimation();
    super.initState();
  }

  @override
  void dispose() {
    animController.dispose();
    super.dispose();
  }

  void startAnimation() {
    Future.delayed(const Duration(milliseconds: 700), () {
      animController.forward();
    });
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Material(
          child: FittedBox(
            child: Padding(
              padding: EdgeInsets.all(30),
              child: Column(
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: FittedBox(
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: 30,
                            vertical: constraints.maxWidth >= 800 ? 90 : 30),
                        child: constraints.maxWidth >= 800
                            ? Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                    SlideTransition(
                                      position: leftpaneloffset,
                                      child: MouseRegion(
                                        onEnter: (event) {
                                          setState(() {
                                            hover = true;
                                          });
                                        },
                                        onExit: (event) {
                                          setState(() {
                                            hover = false;
                                          });
                                        },
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.of(context)
                                                .pushNamed("/scriptcommands");
                                          },
                                          child: AnimatedContainer(
                                            duration: const Duration(
                                                milliseconds: 300),
                                            height: 150,
                                            width: 150,
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(15),
                                                border: Border.all(
                                                    width: 1,
                                                    color: hover
                                                        ? Color(0xFF0ECF6F)
                                                        : Colors.transparent),
                                                boxShadow: [
                                                  BoxShadow(
                                                      color: hover
                                                          ? Colors.transparent
                                                          : Color(0xFF0EA359)
                                                              .withOpacity(0.3),
                                                      blurRadius: 15,
                                                      offset:
                                                          const Offset(0, 0)),
                                                ]),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Icon(
                                                  Icons.code,
                                                  color: Colors.black,
                                                  size: 50,
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                SizedBox(
                                                  width: 80,
                                                  child: Text(
                                                    "Script Komutları",
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 17,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        shadows: [
                                                          Shadow(
                                                              color: Colors
                                                                  .white
                                                                  .withOpacity(
                                                                      0.8),
                                                              blurRadius: 20,
                                                              offset:
                                                                  const Offset(
                                                                      0, 0))
                                                        ]),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 100,
                                    ),
                                    SlideTransition(
                                      position: rightpaneloffset,
                                      child: MouseRegion(
                                        onEnter: (event) {
                                          setState(() {
                                            hover2 = true;
                                          });
                                        },
                                        onExit: (event) {
                                          setState(() {
                                            hover2 = false;
                                          });
                                        },
                                        child: GestureDetector(
                                          onTap: () {
                                            Navigator.of(context)
                                                .pushNamed("/equipments");
                                          },
                                          child: AnimatedContainer(
                                            duration: const Duration(
                                                milliseconds: 300),
                                            height: 150,
                                            width: 150,
                                            decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(15),
                                                border: Border.all(
                                                    color: hover2
                                                        ? Color(0xFF1335B1)
                                                        : Colors.transparent),
                                                boxShadow: [
                                                  BoxShadow(
                                                      color: hover2
                                                          ? Colors.transparent
                                                          : Color(0xFF1335B1)
                                                              .withOpacity(0.3),
                                                      blurRadius: 15,
                                                      offset:
                                                          const Offset(0, 0)),
                                                ]),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Icon(
                                                  Icons.settings,
                                                  color: Colors.black,
                                                  size: 50,
                                                  shadows: [
                                                    Shadow(
                                                        color: Colors.white
                                                            .withOpacity(0.6),
                                                        blurRadius: 30,
                                                        offset:
                                                            const Offset(0, 0))
                                                  ],
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Text(
                                                  "Ekipmanlar",
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 17,
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ])
                            : Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                    SlideTransition(
                                      position: leftpaneloffset,
                                      child: GestureDetector(
                                        onTap: () {
                                          Navigator.of(context)
                                              .pushNamed("/scriptcommands");
                                        },
                                        child: AnimatedContainer(
                                          duration:
                                              const Duration(milliseconds: 300),
                                          height: 150,
                                          width: 150,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                              border: Border.all(
                                                  width: 1,
                                                  color: hover
                                                      ? Color(0xFF0ECF6F)
                                                      : Colors.transparent),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: hover
                                                        ? Colors.transparent
                                                        : Color(0xFF0EA359)
                                                            .withOpacity(0.3),
                                                    blurRadius: 15,
                                                    offset: const Offset(0, 0)),
                                              ]),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.code,
                                                color: Colors.black,
                                                size: 50,
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              SizedBox(
                                                width: 80,
                                                child: Text(
                                                  "Script Komutları",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 17,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      shadows: [
                                                        Shadow(
                                                            color: Colors.white
                                                                .withOpacity(
                                                                    0.8),
                                                            blurRadius: 20,
                                                            offset:
                                                                const Offset(
                                                                    0, 0))
                                                      ]),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 100,
                                    ),
                                    SlideTransition(
                                      position: rightpaneloffset,
                                      child: GestureDetector(
                                        onTap: () {
                                          Navigator.of(context)
                                              .pushNamed("/equipments");
                                        },
                                        child: AnimatedContainer(
                                          duration:
                                              const Duration(milliseconds: 300),
                                          height: 150,
                                          width: 150,
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(15),
                                              border: Border.all(
                                                  color: hover2
                                                      ? Color(0xFF1335B1)
                                                      : Colors.transparent),
                                              boxShadow: [
                                                BoxShadow(
                                                    color: hover2
                                                        ? Colors.transparent
                                                        : Color(0xFF1335B1)
                                                            .withOpacity(0.3),
                                                    blurRadius: 15,
                                                    offset: const Offset(0, 0)),
                                              ]),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.settings,
                                                color: Colors.black,
                                                size: 50,
                                                shadows: [
                                                  Shadow(
                                                      color: Colors.white
                                                          .withOpacity(0.6),
                                                      blurRadius: 30,
                                                      offset:
                                                          const Offset(0, 0))
                                                ],
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Text(
                                                "Ekipmanlar",
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 17,
                                                  fontWeight: FontWeight.w500,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    )
                                  ]),
                      ),
                    ),
                  ),
                  SlideTransition(
                    position: logooffset,
                    child: BottomLogo(),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
