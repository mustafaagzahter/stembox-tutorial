import 'package:flutter/material.dart';

class SearchCategoryProvider extends ChangeNotifier {
  String? _value = "";

  String? get getValue => _value;

  set setValue(String? value) {
    _value = value;
    notifyListeners();
  }
}
