import 'package:flutter/material.dart';

class EditorModeData extends ChangeNotifier {
  bool _editorMode = false;

  bool get mode => _editorMode;

  set mode(bool value) {
    _editorMode = value;
    notifyListeners();
  }
}
