import 'package:flutter/material.dart';

class PhotoUploadButtonImage extends ChangeNotifier {
  String? _base64Image;

  String? get getBase64Image => _base64Image;

  set setBase64Image(String? value) {
    _base64Image = value;
    notifyListeners();
  }
}
