import 'package:flutter/material.dart';
import 'package:stembox_tutorial/screens/equipment_viewer_screen/views/equipment_viewer_editor_view.dart';
import 'package:stembox_tutorial/screens/equipment_viewer_screen/views/equipment_viewer_view.dart';

class EquipmentsViewerScreen extends StatelessWidget {
  final data;
  final editorMode;
  final base64Image;
  const EquipmentsViewerScreen(
      {Key? key, this.data, this.editorMode, this.base64Image})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return editorMode
        ? EquipmentViewerEditorView(data: data, base64Image: base64Image)
        : EquipmentViewerView(data: data, base64Image: base64Image);
  }
}
