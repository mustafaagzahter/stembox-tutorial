import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:code_text_field/code_text_field.dart';
import 'package:flutter/material.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:provider/provider.dart';
import 'package:stembox_tutorial/screens/providers/editor_mode_provider.dart';

import 'package:highlight/languages/python.dart';
import 'package:flutter_highlight/themes/monokai-sublime.dart';

class ScriptCommandEditorView extends StatefulWidget {
  const ScriptCommandEditorView({Key? key}) : super(key: key);

  @override
  _ScriptCommandEditorViewState createState() =>
      _ScriptCommandEditorViewState();
}

class _ScriptCommandEditorViewState extends State<ScriptCommandEditorView> {
  HtmlEditorController htmlEditorController = HtmlEditorController();
  CodeController _codeController = CodeController(language: python);
  String title = "";
  String contentTitle = "";

  String appBarText = "Script Komutları";

  @override
  void initState() {
    title = appBarText;
    super.initState();
  }

  @override
  void dispose() {
    _codeController.dispose();
    super.dispose();
  }

  void control(editorMode) {
    var errorMessages = [];
    if (contentTitle.length < 2) {
      errorMessages.add("Başlık en az 2 karakter olmalıdır!");
    }

    if (errorMessages.length == 0) {
      saveFirebase(editorMode);
    } else {
      for (var i = 0; i < errorMessages.length; i++) {
        Future.delayed(Duration(milliseconds: 500), () {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              behavior: SnackBarBehavior.floating,
              width: 300,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0),
              ),
              content: Text(
                errorMessages[i],
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
              backgroundColor: Colors.red,
              duration: Duration(seconds: 2),
            ),
          );
        });
      }
    }
  }

  void saveFirebase(editorMode) async {
    var txt = await htmlEditorController.getText();
    var codeContent = _codeController.text;
    var command = contentTitle.replaceAll(" ", "");

    FirebaseFirestore.instance
        .collection("modules")
        .doc("scriptCommands")
        .collection("commands")
        .doc(command)
        .set({
          "title": contentTitle,
          "content": txt,
          "code_content": codeContent,
        })
        .then((value) => editorMode.mode = false)
        .then((value) => ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                behavior: SnackBarBehavior.floating,
                width: 200,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.0),
                ),
                content: Text(
                  "Eklendi",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
                backgroundColor: Colors.green,
                duration: Duration(seconds: 2),
              ),
            ));
  }

  // void loadFirebase() async {
  //   var txt = await FirebaseFirestore.instance
  //       .collection("modules")
  //       .doc("scriptCommands")
  //       .collection("commands")
  //       .doc("a")
  //       .get();

  //   print(txt["content"]);

  //   setState(() {
  //     htmlEditorController.setText(txt["content"]);
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    final _editorMode = Provider.of<EditorModeData>(context);

    return LayoutBuilder(
      builder: (context, constraints) {
        return Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              control(_editorMode);
            },
            elevation: 10,
            backgroundColor: Colors.green,
            child: Icon(Icons.save, size: 40, color: Colors.white),
          ),
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(120.0),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: constraints.maxWidth >= 800 ? 60.0 : 10,
                  vertical: constraints.maxWidth >= 800 ? 20.0 : 10),
              child: Container(
                height: 120,
                decoration: BoxDecoration(
                  color: Color(0xFF577bff),
                  borderRadius: BorderRadius.circular(15.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.2),
                      blurRadius: 14.0,
                      offset: const Offset(0, 3),
                    ),
                  ],
                ),
                child: FittedBox(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8),
                              child: IconButton(
                                  style: ButtonStyle(
                                    shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                    ),
                                  ),
                                  onPressed: () => _editorMode.mode = false,
                                  icon: Icon(
                                    Icons.close,
                                    color: Colors.white,
                                    size: 45,
                                  )),
                            ),
                            Container(
                              height: 45,
                              width: 2,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 30,
                            ),
                          ],
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(right: 20, left: 5),
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Text(
                                title.replaceAll(" ", ""),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ),

                        // Padding(
                        //   padding: const EdgeInsets.all(8.0),
                        //   child: IconButton(
                        //     onPressed: () {},
                        //     style: ButtonStyle(
                        //       shape: MaterialStateProperty.all(
                        //         RoundedRectangleBorder(
                        //           borderRadius: BorderRadius.circular(15.0),
                        //         ),
                        //       ),
                        //     ),
                        //     icon: Icon(
                        //       Icons.search,
                        //       color: Colors.white,
                        //       size: 45,
                        //     ),
                        //   ),
                        // ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: constraints.maxWidth >= 800 ? 150 : 10,
                  vertical: constraints.maxWidth >= 800 ? 30 : 10),
              child: Container(
                width: double.infinity,
                padding: EdgeInsets.all(constraints.maxWidth >= 800 ? 100 : 20),
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.2),
                      blurRadius: 8.0,
                      offset: const Offset(0, 3),
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: TextField(
                        style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.w800,
                            color: Colors.grey[800]),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "Başlık Girmek İçin Tıklayınız",
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.w500,
                            fontSize: 30,
                          ),
                        ),
                        onChanged: (value) {
                          setState(() {
                            contentTitle = value;
                            title = "$appBarText/" + value;
                          });
                        },
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: 2,
                      color: Colors.grey,
                    ),
                    HtmlEditor(
                      controller: htmlEditorController,
                      htmlEditorOptions: HtmlEditorOptions(
                        hint: "İçerik bilgisini yazınız.",
                        adjustHeightForKeyboard: true,
                        androidUseHybridComposition: true,
                        autoAdjustHeight: true,
                      ),
                      htmlToolbarOptions: HtmlToolbarOptions(
                        toolbarPosition: ToolbarPosition.aboveEditor,
                        toolbarType: ToolbarType.nativeGrid,
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CodeTheme(
                      data: const CodeThemeData(styles: monokaiSublimeTheme),
                      child: CodeField(
                        textStyle: const TextStyle(fontFamily: 'SourceCode'),
                        controller: _codeController,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
