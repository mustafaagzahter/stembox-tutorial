import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stembox_tutorial/screens/providers/editor_mode_provider.dart';
import 'package:stembox_tutorial/widgets/bottom_logo.dart';

import 'package:stembox_tutorial/widgets/script_command_item_widget.dart';
import 'package:stembox_tutorial/widgets/searchBar.dart';

class ScriptCommandView extends StatefulWidget {
  const ScriptCommandView({Key? key}) : super(key: key);

  @override
  _ScriptCommandViewState createState() => _ScriptCommandViewState();
}

class _ScriptCommandViewState extends State<ScriptCommandView> {
  bool devMode = false;
  bool searchBar = false;
  String searchText = "";
  FocusNode serachBarfocusNode = FocusNode();

  @override
  void initState() {
    loadSettings();
    super.initState();
  }

  Future<void> loadSettings() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      devMode = prefs.getBool("devMode") ?? false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _editorMode = Provider.of<EditorModeData>(context);
    return LayoutBuilder(
      builder: (context, constraints) {
        return Scaffold(
          floatingActionButton: devMode
              ? FloatingActionButton(
                  onPressed: () {
                    _editorMode.mode = true;
                  },
                  elevation: 10,
                  backgroundColor: Color(0xFF577bff),
                  child: Icon(Icons.add, size: 40, color: Colors.white),
                )
              : null,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(120.0),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: constraints.maxWidth >= 800 ? 60.0 : 10,
                  vertical: constraints.maxWidth >= 800 ? 20.0 : 10),
              child: !searchBar
                  ? Container(
                      height: 120,
                      decoration: BoxDecoration(
                        color: Color(0xFF1BD883),
                        borderRadius: BorderRadius.circular(15.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            blurRadius: 14.0,
                            offset: const Offset(0, 3),
                          ),
                        ],
                      ),
                      child: FittedBox(
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8),
                                    child: IconButton(
                                        style: ButtonStyle(
                                          shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                            ),
                                          ),
                                        ),
                                        onPressed: () =>
                                            Navigator.of(context).pop(),
                                        icon: Icon(
                                          Icons.arrow_back_ios_new,
                                          color: Colors.white,
                                          size: 45,
                                        )),
                                  ),
                                  Container(
                                    height: 45,
                                    width: 2,
                                    color: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 30,
                                  ),
                                ],
                              ),
                              Expanded(
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 5),
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Text(
                                      "Script Komutları",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      searchBar = !searchBar;
                                    });
                                    serachBarfocusNode.requestFocus();
                                  },
                                  style: ButtonStyle(
                                    shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                    ),
                                  ),
                                  icon: Icon(
                                    Icons.search,
                                    color: Colors.white,
                                    size: 45,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  : searchBarWidget(
                      onpressed: () {
                        setState(() {
                          searchBar = !searchBar;
                        });
                        serachBarfocusNode.unfocus();
                        searchText = "";
                      },
                      onchanged: (value) {
                        setState(() {
                          searchText = value;
                        });
                      },
                      serachBarfocusNode: serachBarfocusNode,
                      searchBar: searchBar,
                      searchText: searchText,
                    ),
            ),
          ),
          body: Padding(
            padding: EdgeInsets.all(30),
            child: Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: constraints.maxWidth >= 800 ? 60 : 10),
                    child: StreamBuilder(
                      stream: FirebaseFirestore.instance
                          .collection('modules')
                          .doc("scriptCommands")
                          .collection("commands")
                          .orderBy("title")
                          .startAt([searchText]).endAt(
                              [searchText + '\uf8ff']).snapshots(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          final messages = snapshot.data!.docs;
                          if (messages.isEmpty) {
                            return Center(
                              child: Text(
                                'Komut bulunamadı',
                                style: TextStyle(
                                    color: Colors.grey[500],
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500),
                              ),
                            );
                          }
                          return GridView.builder(
                            itemCount: messages.length,
                            gridDelegate:
                                SliverGridDelegateWithMaxCrossAxisExtent(
                              maxCrossAxisExtent: 300,
                              mainAxisSpacing: 50,
                              crossAxisSpacing: 60,
                              childAspectRatio: 2.5,
                            ),
                            itemBuilder: (context, index) {
                              return AnimationConfiguration.staggeredGrid(
                                position: index,
                                duration: const Duration(milliseconds: 500),
                                columnCount: 1,
                                child: ScaleAnimation(
                                  child: FadeInAnimation(
                                      child: ScriptCommandItemWidget(
                                          snapshot: messages[index],
                                          devMode: devMode)),
                                ),
                              );
                            },
                          );
                        } else if (snapshot.hasError) {
                          return Center(
                            child: Text(
                              'Hata oluştu: ${snapshot.error}',
                              style: TextStyle(color: Colors.white),
                            ),
                          );
                        } else {
                          return Center(
                            child: CircularProgressIndicator(
                              color: Colors.green,
                            ),
                          );
                        }
                      },
                    ),
                  ),
                ),
                BottomLogo()
              ],
            ),
          ),
        );
      },
    );
  }
}
