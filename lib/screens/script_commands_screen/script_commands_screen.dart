import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stembox_tutorial/screens/providers/editor_mode_provider.dart';

import 'views/script_command_editor_view.dart';
import 'views/script_commands_view.dart';

class ScriptCommandsScreen extends StatefulWidget {
  const ScriptCommandsScreen({Key? key}) : super(key: key);

  @override
  _ScriptCommandsScreenState createState() => _ScriptCommandsScreenState();
}

class _ScriptCommandsScreenState extends State<ScriptCommandsScreen> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => EditorModeData(),
      child: Consumer<EditorModeData>(
        builder: (context, editorModeData, child) {
          final editorMode = editorModeData.mode;

          return editorMode ? ScriptCommandEditorView() : ScriptCommandView();
        },
      ),
    );
  }
}
