import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:code_text_field/code_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:html_editor_enhanced/html_editor.dart';

import 'package:highlight/languages/python.dart';
import 'package:flutter_highlight/themes/monokai-sublime.dart';

class ScriptCommandViewerView extends StatefulWidget {
  final data;
  const ScriptCommandViewerView({Key? key, required this.data})
      : super(key: key);

  @override
  _ScriptCommandViewerViewState createState() =>
      _ScriptCommandViewerViewState();
}

class _ScriptCommandViewerViewState extends State<ScriptCommandViewerView> {
  HtmlEditorController htmlEditorController = HtmlEditorController();
  CodeController _codeController = CodeController(language: python);

  String title = "";
  String contentTitle = "";
  String content = "";
  String appBarText = "Script Komutları";

  bool load = false;

  @override
  void initState() {
    title = appBarText;
    loadFirebase();
    super.initState();
  }

  @override
  void dispose() {
    _codeController.dispose();
    super.dispose();
  }

  Future<void> loadFirebase() async {
    var command = widget.data.toString().replaceAll(" ", "");

    FirebaseFirestore.instance
        .collection("modules")
        .doc("scriptCommands")
        .collection("commands")
        .doc(command)
        .get()
        .then((value) {
      setState(() {
        _codeController.text = value["code_content"];
        title = appBarText + "/" + command;
        // htmlEditorController.setText(value["content"]);
        content = value["content"];
      });
    }).then((value) => Future.delayed(Duration(milliseconds: 100), () {
              setState(() {
                load = true;
              });
            }));
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return Stack(
          children: [
            Scaffold(
              appBar: PreferredSize(
                preferredSize: Size.fromHeight(120.0),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: constraints.maxWidth >= 800 ? 60.0 : 10,
                      vertical: constraints.maxWidth >= 800 ? 20.0 : 10),
                  child: Container(
                    height: 120,
                    decoration: BoxDecoration(
                      color: Color(0xFF577bff),
                      borderRadius: BorderRadius.circular(15.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          blurRadius: 14.0,
                          offset: const Offset(0, 3),
                        ),
                      ],
                    ),
                    child: FittedBox(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8),
                              child: IconButton(
                                  style: ButtonStyle(
                                    shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                    ),
                                  ),
                                  onPressed: () => Navigator.of(context).pop(),
                                  icon: Icon(
                                    Icons.arrow_back_ios_new,
                                    color: Colors.white,
                                    size: 45,
                                  )),
                            ),
                            Container(
                              height: 45,
                              width: 2,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 30,
                            ),
                            Expanded(
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(right: 20, left: 5),
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Text(
                                    title.replaceAll(" ", ""),
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              body: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: constraints.maxWidth >= 800 ? 150 : 10,
                      vertical: constraints.maxWidth >= 800 ? 30 : 10),
                  child: Container(
                    width: double.infinity,
                    padding:
                        EdgeInsets.all(constraints.maxWidth >= 800 ? 100 : 20),
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          blurRadius: 8.0,
                          offset: const Offset(0, 3),
                        ),
                      ],
                    ),
                    child: Column(
                      children: [
                        Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 10),
                            child: Container(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                widget.data,
                                style: TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.w800,
                                    color: Colors.grey[800]),
                              ),
                            )),
                        Container(
                          width: double.infinity,
                          height: 2,
                          color: Colors.grey,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 20, horizontal: 10),
                          child: HtmlWidget(content),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        SingleChildScrollView(
                          child: CodeTheme(
                            data: const CodeThemeData(
                                styles: monokaiSublimeTheme),
                            child: CodeField(
                              enabled: false,
                              focusNode: FocusNode(),
                              textStyle:
                                  const TextStyle(fontFamily: 'SourceCode'),
                              controller: _codeController,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            if (!load)
              Container(
                color: Colors.white,
                child: Center(
                  child: CircularProgressIndicator(
                    color: Colors.blue,
                  ),
                ),
              ),
          ],
        );
      },
    );
  }
}
