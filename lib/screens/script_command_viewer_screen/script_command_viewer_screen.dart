import 'package:flutter/material.dart';
import 'views/script_command_viewer_editor_view.dart';
import 'views/script_command_viewer_view.dart';

class ScriptCommandViewerScreen extends StatelessWidget {
  final data;
  final editorMode;
  const ScriptCommandViewerScreen({Key? key, this.data, this.editorMode})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return editorMode
        ? ScriptCommandViewerEditorView(data: data)
        : ScriptCommandViewerView(data: data);
  }
}
