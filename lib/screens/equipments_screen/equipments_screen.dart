import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stembox_tutorial/screens/equipments_screen/views/equipments_editor_view.dart';
import 'package:stembox_tutorial/screens/equipments_screen/views/equipments_view.dart';
import 'package:stembox_tutorial/screens/providers/editor_mode_provider.dart';
import 'package:stembox_tutorial/screens/providers/photo_button_image_provider.dart';
import 'package:stembox_tutorial/screens/providers/search_category_provider.dart';

class EquipmentsScreen extends StatelessWidget {
  const EquipmentsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => EditorModeData()),
        ChangeNotifierProvider(create: (context) => PhotoUploadButtonImage()),
        ChangeNotifierProvider(create: (context) => SearchCategoryProvider()),
      ],
      child: Consumer<EditorModeData>(
        builder: (context, editorModeData, child) {
          final editorMode = editorModeData.mode;

          return editorMode ? EquipmentsEditorView() : EquipmentsView();
        },
      ),
    );
  }
}
