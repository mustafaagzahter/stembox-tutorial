import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:stembox_tutorial/screens/providers/editor_mode_provider.dart';
import 'package:stembox_tutorial/screens/providers/search_category_provider.dart';
import 'package:stembox_tutorial/widgets/bottom_logo.dart';
import 'package:stembox_tutorial/widgets/equipment_item_widget.dart';
import 'package:stembox_tutorial/widgets/searchBar.dart';

class EquipmentsView extends StatefulWidget {
  const EquipmentsView({Key? key}) : super(key: key);

  @override
  _EquipmentsViewState createState() => _EquipmentsViewState();
}

class _EquipmentsViewState extends State<EquipmentsView> {
  bool devMode = false;
  bool searchBar = false;
  String searchText = "";
  FocusNode serachBarfocusNode = FocusNode();
  List<PopupMenuEntry<String>>? categoryItems = [];
  @override
  void initState() {
    loadSettings();
    loadCategory();
    super.initState();
  }

  void loadCategory() async {
    categoryItems?.clear();
    var category = await FirebaseFirestore.instance
        .collection("system")
        .doc("equipments")
        .collection("categories")
        .get();

    for (var doc in category.docs) {
      setState(() {
        categoryItems?.add(
          PopupMenuItem<String>(
            child: Text(doc["categori"]),
            value: doc["categori"],
          ),
        );
      });
    }
  }

  Future<void> loadSettings() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      devMode = prefs.getBool("devMode") ?? false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _editorMode = Provider.of<EditorModeData>(context);
    return LayoutBuilder(
      builder: (context, constraints) {
        return Scaffold(
          floatingActionButton: devMode
              ? FloatingActionButton(
                  onPressed: () {
                    _editorMode.mode = true;
                  },
                  elevation: 10,
                  backgroundColor: Color(0xFF577bff),
                  child: Icon(Icons.add, size: 40, color: Colors.white),
                )
              : null,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(120.0),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: constraints.maxWidth >= 800 ? 60.0 : 10,
                  vertical: constraints.maxWidth >= 800 ? 20.0 : 10),
              child: !searchBar
                  ? Container(
                      height: 120,
                      decoration: BoxDecoration(
                        color: Color(0xFF4177d4),
                        borderRadius: BorderRadius.circular(15.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            blurRadius: 14.0,
                            offset: const Offset(0, 3),
                          ),
                        ],
                      ),
                      child: FittedBox(
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.all(8),
                                    child: IconButton(
                                        style: ButtonStyle(
                                          shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(15.0),
                                            ),
                                          ),
                                        ),
                                        onPressed: () =>
                                            Navigator.of(context).pop(),
                                        icon: Icon(
                                          Icons.arrow_back_ios_new,
                                          color: Colors.white,
                                          size: 45,
                                        )),
                                  ),
                                  Container(
                                    height: 45,
                                    width: 2,
                                    color: Colors.white,
                                  ),
                                  SizedBox(
                                    width: 30,
                                  ),
                                ],
                              ),
                              Expanded(
                                child: Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 5),
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Text(
                                      "Ekipmanlar",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: IconButton(
                                  onPressed: () {
                                    setState(() {
                                      searchBar = !searchBar;
                                    });
                                    serachBarfocusNode.requestFocus();
                                  },
                                  style: ButtonStyle(
                                    shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                    ),
                                  ),
                                  icon: Icon(
                                    Icons.search,
                                    color: Colors.white,
                                    size: 45,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  : searchBarWidget(
                      onpressed: () {
                        setState(() {
                          searchBar = !searchBar;
                        });
                        serachBarfocusNode.unfocus();
                        searchText = "";
                      },
                      onchanged: (value) {
                        setState(() {
                          searchText = value;
                        });
                      },
                      serachBarfocusNode: serachBarfocusNode,
                      searchBar: searchBar,
                      searchText: searchText,
                      category: true,
                      categoryItems: categoryItems,
                    ),
            ),
          ),
          body: Padding(
            padding: EdgeInsets.all(30),
            child: Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: constraints.maxWidth >= 800 ? 60 : 10),
                    child: Consumer<SearchCategoryProvider>(
                      builder: (context, value, child) {
                        return StreamBuilder(
                          stream: value.getValue == ""
                              ? FirebaseFirestore.instance
                                  .collection('modules')
                                  .doc("equipments")
                                  .collection("equipments")
                                  .orderBy("title")
                                  .startAt([searchText]).endAt(
                                      [searchText + '\uf8ff']).snapshots()
                              : FirebaseFirestore.instance
                                  .collection('modules')
                                  .doc("equipments")
                                  .collection("equipments")
                                  .where("category", isEqualTo: value.getValue)
                                  .snapshots(),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              final messages = snapshot.data!.docs;
                              if (messages.isEmpty) {
                                return Center(
                                  child: Text(
                                    'Komut bulunamadı',
                                    style: TextStyle(
                                        color: Colors.grey[500],
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500),
                                  ),
                                );
                              }
                              return GridView.builder(
                                itemCount: messages.length,
                                gridDelegate:
                                    SliverGridDelegateWithMaxCrossAxisExtent(
                                  maxCrossAxisExtent: 300,
                                  mainAxisSpacing: 50,
                                  crossAxisSpacing: 60,
                                  childAspectRatio: 2.8,
                                ),
                                itemBuilder: (context, index) {
                                  return AnimationConfiguration.staggeredGrid(
                                    position: index,
                                    duration: const Duration(milliseconds: 500),
                                    columnCount: 1,
                                    child: ScaleAnimation(
                                      child: FadeInAnimation(
                                        child: EquipmentItemWidget(
                                          snapshot: messages[index],
                                          devMode: devMode,
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              );
                            } else if (snapshot.hasError) {
                              return Center(
                                child: Text(
                                  'Hata oluştu: ${snapshot.error}',
                                  style: TextStyle(color: Colors.white),
                                ),
                              );
                            } else {
                              return Center(
                                child: CircularProgressIndicator(
                                  color: Colors.blue[600],
                                ),
                              );
                            }
                          },
                        );
                      },
                    ),
                  ),
                ),
                BottomLogo()
              ],
            ),
          ),
        );
      },
    );
  }
}
