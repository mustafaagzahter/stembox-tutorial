import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:code_text_field/code_text_field.dart';
import 'package:flutter/material.dart';
import 'package:html_editor_enhanced/html_editor.dart';
import 'package:provider/provider.dart';

import 'package:highlight/languages/python.dart';
import 'package:flutter_highlight/themes/monokai-sublime.dart';
import 'package:stembox_tutorial/screens/providers/editor_mode_provider.dart';
import 'package:stembox_tutorial/screens/providers/photo_button_image_provider.dart';
import 'package:stembox_tutorial/widgets/equipments_add_category.dart';
import 'package:stembox_tutorial/widgets/photo_upload_button_widget.dart';

class EquipmentsEditorView extends StatefulWidget {
  const EquipmentsEditorView({Key? key}) : super(key: key);

  @override
  _EquipmentsEditorViewState createState() => _EquipmentsEditorViewState();
}

class _EquipmentsEditorViewState extends State<EquipmentsEditorView> {
  HtmlEditorController htmlEditorController = HtmlEditorController();
  CodeController _codeController = CodeController(language: python);
  String title = "";
  String contentTitle = "";

  String appBarText = "Ekipmanlar";

  String? _dropdownValue = "null";
  bool isCategory = false;
  List<DropdownMenuItem<String>>? categoryItems = [];

  @override
  void initState() {
    title = appBarText;
    final _provider = context.read<PhotoUploadButtonImage>();
    _provider.setBase64Image = "";
    loadCategory();

    super.initState();
  }

  @override
  void dispose() {
    _codeController.dispose();
    super.dispose();
  }

  void loadCategory() async {
    categoryItems?.clear();
    var category = await FirebaseFirestore.instance
        .collection("system")
        .doc("equipments")
        .collection("categories")
        .get();

    for (var doc in category.docs) {
      setState(() {
        categoryItems?.add(
          DropdownMenuItem<String>(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(doc["categori"]),
                SizedBox(
                  width: 20,
                ),
                //delere button
                IconButton(
                  onPressed: () {
                    FirebaseFirestore.instance
                        .collection("system")
                        .doc("equipments")
                        .collection("categories")
                        .doc(doc.id)
                        .delete();

                    setState(() {
                      categoryItems?.removeWhere(
                          (element) => element.value == doc["categori"]);
                      _dropdownValue = "null";
                    });
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        behavior: SnackBarBehavior.floating,
                        width: 200,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                        content: Text(
                          "Kategori Silindi",
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white),
                        ),
                        backgroundColor: Colors.red,
                        duration: Duration(seconds: 2),
                      ),
                    );
                  },
                  icon: Icon(Icons.delete),
                ),
              ],
            ),
            value: doc["categori"],
          ),
        );
      });
    }
  }

  void control(editorMode, photoUploadButtonImage) {
    var errorMessages = [];
    if (contentTitle.length < 2) {
      errorMessages.add("Başlık en az 2 karakter olmalıdır!");
    }
    if (_dropdownValue == "null") {
      errorMessages.add("Lütfen bir kategori seçiniz!");
    }

    if (errorMessages.length == 0) {
      saveFirebase(editorMode, photoUploadButtonImage);
    } else {
      for (var i = 0; i < errorMessages.length; i++) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            behavior: SnackBarBehavior.floating,
            width: 300,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0),
            ),
            content: Text(
              errorMessages[i],
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: Colors.red,
            duration: Duration(milliseconds: 1500),
          ),
        );
      }
    }
  }

  void saveFirebase(editorMode, photoUploadButtonImage) async {
    var txt = await htmlEditorController.getText();
    var codeContent = _codeController.text;
    var equipment = contentTitle.replaceAll(" ", "");
    var base64Image = photoUploadButtonImage.getBase64Image;

    FirebaseFirestore.instance
        .collection("modules")
        .doc("equipments")
        .collection("equipments")
        .doc(equipment)
        .set({
          "title": contentTitle,
          "content": txt,
          "code_content": codeContent,
          "image": base64Image == null ? "" : base64Image,
          "category": _dropdownValue,
        })
        .then((value) => editorMode.mode = false)
        .then((value) => ScaffoldMessenger.of(context).showSnackBar(
              SnackBar(
                behavior: SnackBarBehavior.floating,
                width: 200,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25.0),
                ),
                content: Text(
                  "Eklendi",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
                backgroundColor: Colors.green,
                duration: Duration(seconds: 2),
              ),
            ));
  }

  @override
  Widget build(BuildContext context) {
    final _editorMode = Provider.of<EditorModeData>(context);
    final _photoUploadButtonImage =
        Provider.of<PhotoUploadButtonImage>(context);

    return LayoutBuilder(
      builder: (context, constraints) {
        return Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              control(_editorMode, _photoUploadButtonImage);
            },
            elevation: 10,
            backgroundColor: Colors.green,
            child: Icon(Icons.save, size: 40, color: Colors.white),
          ),
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(120.0),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: constraints.maxWidth >= 800 ? 60.0 : 10,
                  vertical: constraints.maxWidth >= 800 ? 20.0 : 10),
              child: Container(
                height: 120,
                decoration: BoxDecoration(
                  color: Color(0xFF577bff),
                  borderRadius: BorderRadius.circular(15.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.2),
                      blurRadius: 14.0,
                      offset: const Offset(0, 3),
                    ),
                  ],
                ),
                child: FittedBox(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8),
                              child: IconButton(
                                  style: ButtonStyle(
                                    shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                    ),
                                  ),
                                  onPressed: () => _editorMode.mode = false,
                                  icon: Icon(
                                    Icons.close,
                                    color: Colors.white,
                                    size: 45,
                                  )),
                            ),
                            Container(
                              height: 45,
                              width: 2,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 30,
                            ),
                          ],
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.only(right: 20, left: 5),
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Text(
                                title.replaceAll(" ", ""),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
          body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: constraints.maxWidth >= 800 ? 150 : 10,
                  vertical: constraints.maxWidth >= 800 ? 30 : 10),
              child: Container(
                width: double.infinity,
                padding: EdgeInsets.all(constraints.maxWidth >= 800 ? 100 : 20),
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.2),
                      blurRadius: 8.0,
                      offset: const Offset(0, 3),
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 10),
                      child: constraints.maxWidth >= 800
                          ? Row(
                              children: [
                                PhotoUploadButtonWidget(
                                  base64Image: null,
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Expanded(
                                  child: Container(
                                    child: TextField(
                                      style: TextStyle(
                                          fontSize: 30,
                                          fontWeight: FontWeight.w800,
                                          color: Colors.grey[800]),
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText:
                                            "Başlık Girmek İçin Tıklayınız",
                                        hintStyle: TextStyle(
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 30,
                                        ),
                                      ),
                                      onChanged: (value) {
                                        setState(() {
                                          contentTitle = value;
                                          title =
                                              "$appBarText/${_dropdownValue != "null" ? "$_dropdownValue/" : "kategori/"}" +
                                                  value;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                                SizedBox(width: 20),
                                DropdownButton(
                                  value: _dropdownValue,
                                  hint: Text("Kategori Seçiniz"),
                                  items: categoryItems! +
                                      [
                                        DropdownMenuItem(
                                          child: Text("Kategori Seçiniz"),
                                          value: "null",
                                        ),
                                        DropdownMenuItem(
                                          child: Text("Kategori Ekle"),
                                          value: "add",
                                        )
                                      ],
                                  onChanged: (String? selectedValue) async {
                                    if (selectedValue is String &&
                                        selectedValue != "null" &&
                                        selectedValue != "add") {
                                      setState(() {
                                        _dropdownValue = selectedValue;
                                        title = "$appBarText/" +
                                            _dropdownValue
                                                .toString()
                                                .replaceAll(" ", "") +
                                            "/" +
                                            contentTitle.replaceAll(" ", "");
                                      });
                                    } else if (selectedValue == "add") {
                                      var temporary =
                                          await htmlEditorController.getText();

                                      setState(() {
                                        isCategory = true;
                                      });
                                      showAddCategory(context).then((value) {
                                        setState(() {
                                          isCategory = false;
                                          selectedValue = value;
                                        });
                                        print("object   $value");
                                        loadCategory();
                                        Future.delayed(
                                            Duration(milliseconds: 500), () {
                                          htmlEditorController
                                              .setText(temporary);
                                        });
                                      });
                                    }
                                  },
                                ),
                              ],
                            )
                          : Column(
                              children: [
                                PhotoUploadButtonWidget(
                                  base64Image: null,
                                ),
                                SizedBox(
                                  width: 20,
                                ),
                                Container(
                                  child: TextField(
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 30,
                                        fontWeight: FontWeight.w800,
                                        color: Colors.grey[800]),
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: "Başlık Girmek İçin Tıklayınız",
                                      hintStyle: TextStyle(
                                        color: Colors.grey,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 30,
                                      ),
                                    ),
                                    onChanged: (value) {
                                      setState(() {
                                        contentTitle = value;
                                        title =
                                            "$appBarText/${_dropdownValue != "null" ? "$_dropdownValue/" : "kategori/"}" +
                                                value;
                                      });
                                    },
                                  ),
                                ),
                                SizedBox(height: 20),
                                DropdownButton(
                                  value: _dropdownValue,
                                  hint: Text("Kategori Seçiniz"),
                                  items: categoryItems! +
                                      [
                                        DropdownMenuItem(
                                          child: Text("Kategori Seçiniz"),
                                          value: "null",
                                        ),
                                        DropdownMenuItem(
                                          child: Text("Kategori Ekle"),
                                          value: "add",
                                        )
                                      ],
                                  onChanged: (String? selectedValue) async {
                                    if (selectedValue is String &&
                                        selectedValue != "null" &&
                                        selectedValue != "add") {
                                      setState(() {
                                        _dropdownValue = selectedValue;
                                        title = "$appBarText/" +
                                            _dropdownValue
                                                .toString()
                                                .replaceAll(" ", "") +
                                            "/" +
                                            contentTitle.replaceAll(" ", "");
                                      });
                                    } else if (selectedValue == "add") {
                                      var temporary =
                                          await htmlEditorController.getText();

                                      setState(() {
                                        isCategory = true;
                                      });
                                      showAddCategory(context).then((value) {
                                        setState(() {
                                          isCategory = false;
                                          selectedValue = value;
                                        });
                                        print("object   $value");
                                        loadCategory();
                                        Future.delayed(
                                            Duration(milliseconds: 500), () {
                                          htmlEditorController
                                              .setText(temporary);
                                        });
                                      });
                                    }
                                  },
                                ),
                              ],
                            ),
                    ),
                    Container(
                      width: double.infinity,
                      height: 2,
                      color: Colors.grey,
                    ),
                    if (!isCategory)
                      HtmlEditor(
                        controller: htmlEditorController,
                        htmlEditorOptions: HtmlEditorOptions(
                          hint: "İçerik bilgisini yazınız.",
                          adjustHeightForKeyboard: true,
                          androidUseHybridComposition: true,
                          autoAdjustHeight: true,
                        ),
                        htmlToolbarOptions: HtmlToolbarOptions(
                          toolbarPosition: ToolbarPosition.aboveEditor,
                          toolbarType: ToolbarType.nativeGrid,
                        ),
                      )
                    else
                      FittedBox(
                        child: Container(
                          alignment: Alignment.center,
                          height: 500,
                          padding: EdgeInsets.all(80),
                          child: Text("Kategori Ekleme",
                              style: TextStyle(
                                color: Colors.grey[400],
                                fontSize: 80,
                                fontWeight: FontWeight.bold,
                              )),
                        ),
                      ),
                    SizedBox(
                      height: 10,
                    ),
                    CodeTheme(
                      data: const CodeThemeData(styles: monokaiSublimeTheme),
                      child: CodeField(
                        textStyle: const TextStyle(fontFamily: 'SourceCode'),
                        controller: _codeController,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
