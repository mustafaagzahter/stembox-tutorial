import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:stembox_tutorial/screens/equipment_viewer_screen/equipment_viewer_screen.dart';

class EquipmentItemWidget extends StatelessWidget {
  final snapshot;

  final bool devMode;
  const EquipmentItemWidget(
      {super.key, required this.snapshot, required this.devMode});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Color(0xFF7a9cd5),
      borderRadius: BorderRadius.circular(100.0),
      shadowColor: Colors.black.withOpacity(0.2),
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => EquipmentsViewerScreen(
                data: snapshot["title"],
                editorMode: devMode,
                base64Image: snapshot['image'],
              ),
            ),
          );
        },
        borderRadius: BorderRadius.circular(100.0),
        child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(100.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.2),
                blurRadius: 5.0,
                offset: const Offset(0, 3),
              ),
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Row(
              children: [
                Container(
                  height: 80,
                  width: 80,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle,
                  ),
                  child: snapshot['image'] == "" || snapshot['image'] == null
                      ? Icon(
                          Icons.image,
                          color: Colors.grey,
                          size: 40,
                        )
                      : Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: FittedBox(
                            child: HtmlWidget(
                              "<img src='data:image/png;base64,${snapshot['image']}'>",
                            ),
                          ),
                        ),
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      snapshot['title'],
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
