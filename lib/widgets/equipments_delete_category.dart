import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

showDeleteCategory(BuildContext context, categories) {
  String categoryName = "";
  DropdownMenuItem<String> dropdownMenuItem = categories;
  Completer<void> completer = Completer<void>();
  // set up the buttons
  Widget cancelButton = TextButton(
    child: Text("İptal"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );
  // Widget continueButton =

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext _context) {
      return ScaffoldMessenger(child: Builder(builder: (context) {
        return Padding(
          padding: const EdgeInsets.all(50.0),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: AlertDialog(
              title: Text("Kategori Sil"),
              content: DropdownButton<String>(
                value: dropdownMenuItem.value,
                onChanged: (String? newValue) {
                  categoryName = newValue!;
                },
                items: categories,
              ),
              actions: [
                cancelButton,
                TextButton(
                  child: Text("Sil"),
                  onPressed: () {
                    if (categoryName != "") {
                      firebaseCategoryDelete(categoryName, context);
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          behavior: SnackBarBehavior.floating,
                          width: 200,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25.0),
                          ),
                          content: Text(
                            "Kategori Adı Boş Olamaz",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                          backgroundColor: Colors.red,
                          duration: Duration(seconds: 2),
                        ),
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        );
      }));
    },
  ).then((value) => completer.complete());

  return completer.future;
}

firebaseCategoryDelete(String _categoryName, BuildContext context) {
  FirebaseFirestore.instance
      .collection("system")
      .doc("equipments")
      .collection("categories")
      .doc(_categoryName)
      .delete()
      .then((value) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: SnackBarBehavior.floating,
        width: 200,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
        content: Text(
          "Kategori Silindi",
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.green,
        duration: Duration(seconds: 2),
      ),
    );
    Navigator.of(context).pop();
  }).catchError((error) {
    Navigator.of(context).pop();

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: SnackBarBehavior.floating,
        width: 200,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
        content: Text(
          "Kategori Silinemedi",
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.red,
        duration: Duration(seconds: 2),
      ),
    );
  });
}
