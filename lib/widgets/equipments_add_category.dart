import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

showAddCategory(BuildContext context) {
  String categoryName = "";

  Completer<void> completer = Completer<void>();
  // set up the buttons
  Widget cancelButton = TextButton(
    child: Text("İptal"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );
  // Widget continueButton =

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext _context) {
      return ScaffoldMessenger(child: Builder(builder: (context) {
        return Padding(
          padding: const EdgeInsets.all(50.0),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: AlertDialog(
              title: Text("Kategori Ekle"),
              content: TextField(
                onChanged: (value) {
                  categoryName = value;
                },
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Kategori Adı',
                ),
              ),
              actions: [
                cancelButton,
                TextButton(
                  child: Text("Kaydet"),
                  onPressed: () {
                    if (categoryName != "" && categoryName.length > 1) {
                      firebaseCategorySave(categoryName, _context);
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          behavior: SnackBarBehavior.floating,
                          width: 200,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25.0),
                          ),
                          content: Text(
                            "Kategori Adı Boş Olamaz",
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white),
                          ),
                          backgroundColor: Colors.red,
                          duration: Duration(seconds: 2),
                        ),
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        );
      }));
    },
  ).then((value) => completer.complete());

  return completer.future;
}

firebaseCategorySave(String _categoryName, BuildContext context) {
  FirebaseFirestore.instance
      .collection('system')
      .doc('equipments')
      .collection('categories')
      .doc(_categoryName)
      .set({"categori": _categoryName}).then((value) {
    Navigator.of(context).pop();

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        behavior: SnackBarBehavior.floating,
        width: 200,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
        content: Text(
          "Kategori Eklendi",
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.green,
        duration: Duration(seconds: 2),
      ),
    );
  });
}
