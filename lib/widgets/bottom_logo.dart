import 'package:flutter/material.dart';

class BottomLogo extends StatelessWidget {
  const BottomLogo({super.key});

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      child: Container(
        alignment: Alignment.bottomCenter,
        width: 600,
        height: 100,
        child: Image.asset(
          "assets/images/riders_logo.png",
          scale: 1.2,
        ),
      ),
    );
  }
}
