import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:image_picker_web/image_picker_web.dart';
import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:stembox_tutorial/screens/providers/photo_button_image_provider.dart';

class PhotoUploadButtonWidget extends StatefulWidget {
  final base64Image;
  const PhotoUploadButtonWidget({Key? key, required this.base64Image})
      : super(key: key);

  @override
  State<PhotoUploadButtonWidget> createState() =>
      _PhotoUploadButtonWidgetState();
}

class _PhotoUploadButtonWidgetState extends State<PhotoUploadButtonWidget> {
  // final imgPicker.ImagePicker _picker = imgPicker.ImagePicker();
  String? base64Image;
  var provider;
  @override
  void initState() {
    super.initState();
    base64Image = widget.base64Image;
  }

  Future<void> _pickImage(_photoUploadButtonImage) async {
    Uint8List? bytesFromPicker = await ImagePickerWeb.getImageAsBytes();
    if (bytesFromPicker != null) {
      setState(() {
        base64Image = base64Encode(bytesFromPicker);
      });
      _photoUploadButtonImage.setBase64Image = base64Image;
    }

    // final pickedFile = await _picker.pickImage(source:imgPicker.Image.gallery);
    // final pickedFile =
    //     await _picker.pickImage(source: imgPicker.ImageSource.gallery);

    // if (pickedFile != null) {
    //   final bytes = await pickedFile.readAsBytes();
    //   setState(() {
    //     base64Image = base64Encode(bytes);
    //   });
    //   _photoUploadButtonImage.setBase64Image = base64Image;
    // }
  }

  @override
  Widget build(BuildContext context) {
    final photoUploadButtonImage =
        Provider.of<PhotoUploadButtonImage>(context, listen: false);

    provider = photoUploadButtonImage;
    return Material(
      elevation: 3,
      color: Colors.white,
      shape: CircleBorder(),
      child: InkWell(
        borderRadius: BorderRadius.circular(50),
        onTap: () {
          if (base64Image != null && base64Image != "") {
            setState(() {
              base64Image = null;
            });
            photoUploadButtonImage.setBase64Image = "";
          } else {
            _pickImage(photoUploadButtonImage);
          }
        },
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey.shade400,
              width: 2,
            ),
            shape: BoxShape.circle,
          ),
          child: base64Image == null || base64Image == ""
              ? Icon(
                  Icons.image,
                  size: 40,
                  color: Colors.grey[600],
                )
              : Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: FittedBox(
                    child: HtmlWidget(
                      "<img src='data:image/png;base64,$base64Image'>",
                    ),
                  ),
                ),
          height: 100,
          width: 100,
        ),
      ),
    );
  }
}
