import 'package:flutter/material.dart';
import 'package:stembox_tutorial/screens/script_command_viewer_screen/script_command_viewer_screen.dart';

class ScriptCommandItemWidget extends StatelessWidget {
  final snapshot;

  final bool devMode;
  const ScriptCommandItemWidget(
      {super.key, required this.snapshot, required this.devMode});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.grey,
      borderRadius: BorderRadius.circular(20.0),
      shadowColor: Colors.black.withOpacity(0.2),
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => ScriptCommandViewerScreen(
                data: snapshot['title'],
                editorMode: devMode,
              ),
            ),
          );
        },
        borderRadius: BorderRadius.circular(20.0),
        child: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.2),
                blurRadius: 5.0,
                offset: const Offset(0, 3),
              ),
            ],
          ),
          child: Text(
            snapshot['title'],
            style: TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
