import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stembox_tutorial/screens/providers/search_category_provider.dart';

class searchBarWidget extends StatelessWidget {
  const searchBarWidget({
    Key? key,
    required this.serachBarfocusNode,
    required this.searchBar,
    required this.searchText,
    required this.onchanged,
    required this.onpressed,
    this.category = false,
    this.categoryItems,
  }) : super(key: key);

  final FocusNode serachBarfocusNode;
  final bool searchBar;
  final String searchText;
  final Function(String) onchanged;
  final Function() onpressed;
  final bool? category;
  final List<PopupMenuEntry<String>>? categoryItems;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 120,
      decoration: BoxDecoration(
        color: Color(0xFFa085b5),
        borderRadius: BorderRadius.circular(15.0),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            blurRadius: 14.0,
            offset: const Offset(0, 3),
          ),
        ],
      ),
      child: FittedBox(
        child: Container(
          width: MediaQuery.of(context).size.width,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: Icon(
                      Icons.search,
                      color: Colors.white,
                      size: 45,
                    ),
                  ),
                  Container(
                    height: 45,
                    width: 2,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: category != null && category! ? 0 : 30,
                  ),
                ],
              ),
              if (category != null && category!)
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: PopUpMenu(
                    menuList: categoryItems,
                    icon: Icon(
                      Icons.category_rounded,
                      color: Colors.white,
                      size: 45,
                    ),
                  ),
                ),
              category!
                  ? Expanded(child: Consumer<SearchCategoryProvider>(
                      builder: (context, value, child) {
                        return value.getValue == ""
                            ? SearchBar(
                                focusNode: serachBarfocusNode,
                                hintText: "Komut ara...",
                                onChanged: onchanged,
                                shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15)),
                                )),
                              )
                            : Container(
                                alignment: Alignment.center,
                                height: 55,
                                decoration: BoxDecoration(
                                  color: Color(0xFFefe6f8),
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                child: Text(
                                  value.getValue!,
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey[600]),
                                ),
                              );
                      },
                    ))
                  : Expanded(
                      child: SearchBar(
                        focusNode: serachBarfocusNode,
                        hintText: "Komut ara...",
                        onChanged: onchanged,
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        )),
                      ),
                    ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: IconButton(
                  onPressed: onpressed,
                  style: ButtonStyle(
                    shape: MaterialStateProperty.all(
                      RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                    ),
                  ),
                  icon: Icon(
                    Icons.close,
                    color: Colors.white,
                    size: 45,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// // class searchBarData extends ChangeNotifier {
// //   bool searchBar = false;
// //   String searchText = "";

// //   bool get mode => searchBar;

// //   set mode(bool value) {
// //     searchBar = value;
// //     notifyListeners();
// //   }
// // }

void _showDropdownMenu(BuildContext context) {
  final RenderBox overlay =
      Overlay.of(context).context.findRenderObject() as RenderBox;
  final RenderBox dropdownButton = context.findRenderObject() as RenderBox;
  final RelativeRect position = RelativeRect.fromRect(
    Rect.fromPoints(
      dropdownButton.localToGlobal(Offset.zero, ancestor: overlay),
      dropdownButton.localToGlobal(dropdownButton.size.bottomRight(Offset.zero),
          ancestor: overlay),
    ),
    Offset.zero & overlay.size,
  );

  showMenu<String>(
    context: context,
    position: position,
    items: <PopupMenuEntry<String>>[
      PopupMenuItem<String>(
        value: 'Seçenek 1',
        child: Text('Seçenek 1'),
      ),
      PopupMenuItem<String>(
        value: 'Seçenek 2',
        child: Text('Seçenek 2'),
      ),
      PopupMenuItem<String>(
        value: 'Seçenek 3',
        child: Text('Seçenek 3'),
      ),
    ],
  ).then((value) {
    if (value != null) {}
  });
}

class PopUpMenu extends StatelessWidget {
  final menuList;
  final icon;
  const PopUpMenu({Key? key, required this.menuList, required this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<SearchCategoryProvider>(
      builder: (context, category, child) {
        return PopupMenuButton(
          tooltip: "Kategori Seçin",
          itemBuilder: (context) =>
              <PopupMenuEntry<String>>[
                PopupMenuItem<String>(
                  value: 'all',
                  child: Text('Hepsi'),
                ),
              ] +
              menuList,
          icon: icon,
          onSelected: (value) {
            if (value == "all") {
              category.setValue = "";
            } else {
              category.setValue = value;
            }
          },
        );
      },
    );
  }
}
